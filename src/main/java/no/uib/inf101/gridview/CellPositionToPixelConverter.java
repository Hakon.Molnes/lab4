package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

//Denne regner ut automatisk på griden hvor graphics skal være hen

public class CellPositionToPixelConverter {
  // TODO: Implement this class (Fra oppgavegjennomgang) vi vil kun ha dimensjonene til griden

  private Rectangle2D box;
  private double margin;
  private double cellWidth;
  private double cellHight;
  private GridDimension dim;


  //En klasse som regner ut posisjonene til enkelte celler i en gitt grid-objekt
  public CellPositionToPixelConverter(Rectangle2D gridBox, GridDimension gridDimension, double margin){
    this.box = gridBox;
    this.margin = margin;
    this.dim = gridDimension;

    cellWidth = (box.getWidth() - margin * (gridDimension.cols() + 1)) / gridDimension.cols();
    cellHight = (box.getHeight() - margin * (gridDimension.rows() + 1)) / gridDimension.rows();
  }

  //Nå skal vi regne ut slik at boxene er linet opp med hvordan vi skal tegne gridden \



  public Rectangle2D getBoundsForCell(CellPosition cellPosition){
    double x = box.getX() + margin * (cellPosition.col() + 1) + cellWidth*cellPosition.col();
    double y = box.getY() + margin * (cellPosition.row() + 1) + cellHight*cellPosition.row();

    return new Rectangle2D.Double(x,y, cellWidth , cellHight);
  }

}