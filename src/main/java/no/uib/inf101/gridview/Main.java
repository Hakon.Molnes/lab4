package no.uib.inf101.gridview;

import java.awt.Color;
import javax.swing.JFrame;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;


public class Main {
  public static void main(String[] args) {

    IColorGrid grid = new ColorGrid(6, 6);
    GridView gridvView = new GridView(grid);
    //Cellpositions that match with index
    grid.set(new CellPosition(0, 0), Color.CYAN);
    grid.set(new CellPosition(1, 1), Color.CYAN);
    grid.set(new CellPosition(2, 2), Color.CYAN);
    grid.set(new CellPosition(3, 3), Color.CYAN);
    grid.set(new CellPosition(4, 4), Color.CYAN);
    grid.set(new CellPosition(5, 5), Color.CYAN);
    

    JFrame frame = new JFrame();
    frame.setTitle("WE OFF THE GRIDDY");
    frame.setContentPane(gridvView);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);


    
  }
}