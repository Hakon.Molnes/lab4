package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;


public class GridView extends JPanel {

  //Konstruktør
  //den ytre marginen mellom vinduet og gridden
  private final static double OUTER_MARGIN = 15;
  //den indre mellom cellene i gridden
  private final static double INNER_MARGIN = 15;
  //Fargen til innerMargin
  private final static Color BACKGROUND_COLOR = Color.LIGHT_GRAY; 
  //Selve grid-objektet
  private IColorGrid grid;

  public GridView(IColorGrid grid){
    this.grid = grid;
    this.setPreferredSize(new Dimension(400, 300));
  }

  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    drawgrid(g2);

  }
  private void drawgrid(Graphics2D g2){
    //tegne box som gridden skal tegnes inni
    Rectangle2D gridBox = new Rectangle2D.Double(OUTER_MARGIN, OUTER_MARGIN, 
      this.getWidth() - 2*OUTER_MARGIN, this.getHeight() - 2* OUTER_MARGIN);

    g2.setColor(BACKGROUND_COLOR);
    g2.fill(gridBox);

    //Holder styr på hvor enkelte celler skal tegnes
    CellPositionToPixelConverter converter = new CellPositionToPixelConverter(gridBox, grid, INNER_MARGIN);

    drawCells(g2, grid, converter);
    
  }
  
  private static void drawCells(Graphics2D g2, CellColorCollection grid, CellPositionToPixelConverter converter){
    
    for (CellColor cell : grid.getCells()){
      //tegne cellen
      Rectangle2D cellBox = converter.getBoundsForCell(cell.cellPosition());

      if (cell.color() != null){
        g2.setColor(cell.color());
      }
      else {
        g2.setColor(Color.DARK_GRAY);
      }
      g2.fill(cellBox);
    }
  }

}
