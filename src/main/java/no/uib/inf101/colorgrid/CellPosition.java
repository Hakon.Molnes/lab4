package no.uib.inf101.colorgrid;

// Les om records her: https://inf101.ii.uib.no/notat/mutabilitet/#record

/**
 * A CellPosition consists of a row and a column.
 *
 * @param row  the row of the cell
 * @param col  the column of the cell
 */
public record CellPosition(int row, int col) {
    public CellPosition(int row) {
    this(row, 0); // standard-verdi 0 for kolonne
  }
    public CellPosition nextRow() {
    return new CellPosition(this.row + 1, this.col);
  }
}
