package no.uib.inf101.colorgrid;


import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  //Field declarations for class (Attributes)
  private int rows;
  private int cols;
  private List<CellColor> c_list;
  
  //constructing object ColorGrid
  public ColorGrid(int rows, int cols){
    this.rows = rows;
    this.cols = cols;
    this.c_list = new ArrayList<>();

    //Creating the grid (could have used this.row + 1)
    for (int col = 0; col < cols; col++) {
      for (int row = 0; row < rows; row++) {
          CellPosition pos = new CellPosition(row, col);
          CellColor cellColor = new CellColor(pos, null);
          c_list.add(cellColor);

      }
    }
  }

  public int rows() {
    return rows;
  }
  
  public int cols() {
    return cols;
  }

  //Gets the record, then returns all objects from record "Cellcolor"
  public List<CellColor> getCells() {
    return c_list;
  }


  @Override
  public Color get(CellPosition pos) {
    //Using pos to index the list of Cellcolor 
    int index = pos.row() * cols() + pos.col();
    if (!isLegal(pos)){
      throw new IndexOutOfBoundsException();
    }
    else if (index >= 0 && index <= c_list.size()) {
      return c_list.get(index).color();
    } 

    else {
        throw new IndexOutOfBoundsException("Invalid cell position");
    }

  }


  @Override
  public void set(CellPosition pos, Color color) {
    //set color at given position from 3d-position to 2d-index
    int index = pos.row() * cols() + pos.col();
    if (!isLegal(pos)){
      throw new IndexOutOfBoundsException();
    }

    else if (index >= 0 && index < c_list.size()) {
      c_list.set(index, new CellColor(pos, color));
    } 
    
    else {
        throw new IndexOutOfBoundsException("Invalid cell position");
    }
  }

  public boolean isLegal(CellPosition pos){
    return pos.row() >= 0 && pos.row() < rows && pos.col() >= 0 && pos.col() < cols;  }


}

